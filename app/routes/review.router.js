// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import review middleware
const reviewMiddleware = require("../middlewares/review.middleware");

//Import review controller 
const reviewController = require("../controllers/review.controller");

router.get("/reviews", reviewMiddleware.getAllReviewMiddleware, reviewController.getAllReview)

router.post("/courses/:courseId/reviews", reviewMiddleware.createReviewMiddleware, reviewController.createReviewOfCourse);

router.get("/courses/:courseId/reviews", reviewMiddleware.getAllReviewMiddleware, reviewController.getAllReviewOfCourse);

router.get("/reviews/:reviewId", reviewMiddleware.getDetailReviewMiddleware, reviewController.getReviewById);

router.put("/reviews/:reviewId", reviewMiddleware.updateReviewMiddleware, reviewController.updateReviewByID)

router.delete("/courses/:courseId/reviews/:reviewId", reviewMiddleware.deleteReviewMiddleware, reviewController.deleteReviewByID)

module.exports = router;